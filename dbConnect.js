const mongoose = require("mongoose");

function connectMongoDB() {
    return mongoose.connect('mongodb://localhost:27017/jenycare', { useNewUrlParser: true })
        .catch(error => {
            console.log(error);
            process.exit(1);
        });
}

module.exports = connectMongoDB;