const router = require("express").Router();

// models
const UserModel = require('./models/users');

const token = "ahdfjhasdbfjhasbdfjashbdfjhasbdfjahsbdf";

const users = [];

router.post("/login", (req, res) => {
    const { username, password } = req.body;
    if (username && password) {
        UserModel.findOne({
            username,
            password
        }).then(user => {
            if (user) {
                res.status(200).json({
                    token: token,
                    user: {
                        username,
                        name: user.name
                    }
                })
            } else {
                res.status(400).json({
                    message: "Username or Password incorrect!"
                })
            }
        })



        // const { username, password } = req.body;
        // const user = users.find(user => user.username === username && user.password === password);
        // if (user) {
        //     res.status(200).json({
        //         token: token,
        //         user: {
        //             username,
        //             name: user.name
        //         }
        //     })
        // } else {
        //     res.status(400).json({
        //         message: "Username or Password incorrect!"
        //     })
        // }
    }
})

router.post("/register", (req, res) => {
    const { username, password, name } = req.body;
    if (username && password && name) {
        UserModel.findOne({
            username
        }).then(user => {
            if (user) {
                // user already exist
                res.status(400).json({
                    message: "User already exist"
                })
            } else {
                // we can create user
                UserModel.create({ username, password, name })
                    .then(() => {
                        res.status(201).json({
                            message: "OK"
                        })
                    })
                    .catch(err => {
                        res.status(400).json({
                            message: err.message
                        })
                    })
            }
        })



        // const existUser = users.find(user => user.username === username);
        // if (existUser) {
        //     res.status(400).json({
        //         message: "Username already exists!"
        //     })
        // } else {
        //     users.push({
        //         username,
        //         password,
        //         name
        //     })
        //     console.log(users);

        //     res.status(200).json({
        //         message: "OK"
        //     })
        // }
    } else {
        res.status(400).json({
            message: "Username, Password or Name invalid!"
        })
    }

})

module.exports = router;