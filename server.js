
const express = require('express')
const app = express()
const port = 3001

const apiRouter = require('./api');

// db connect
const dbConnect = require('./dbConnect');

function runApp() {
    app.use(express.json()) // for parsing application/json
    app.use(express.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded
    app.use("/api", apiRouter);
    app.listen(port, () => console.log(`Example app listening on port ${port}!`))
}

dbConnect()
    .then(runApp)
    .catch(err => {
        console.log(err);
        process.exit(1);
    })